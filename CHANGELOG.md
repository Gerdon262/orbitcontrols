# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.0.0] - 2018-10-22
### Added
- Webpack, babel
- webpack config
- babelrc

### Changed
- Using webpack to compile index.js

### Updated
- README
- CHANGELOG 

## [2.3.0] - 2018-09-11
### Changed
- three.js has to be passed to OrbitControls

## [2.2.0] - 2018-09-11
### Changed
- imports

## [2.1.1] - 2018-08-28
### Changed
- Now able to zoom from the outside

## [2.0.1] - 2018-08-28
### Changed
- Moved three dependency to devDependency

## [2.0.0] - 2018-08-28
### Added
- `.idea` and `.git` directory to `.gitignore`
- eslint

### Changed
- All functions to ES6
- `var` to `const`/`let`
- Single function for `console` warning

### Removed
- ES6 from `README`
- Unnecessary returns

## [1.0.6] - 2018-08-28
- Changed naming to kebab-case

## [1.0.5] - 2018-08-27
- Fixed copy pasted date

## [1.0.4] - 2018-08-27
- Cleaned package.json

## [1.0.3] - 2018-08-27
- Initial commit of the OrbitControls

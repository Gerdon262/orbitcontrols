/**
 * http://eslint.org/docs/user-guide/configuring
 */

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  /**
   * Specify build-in environments
   * @doc https://eslint.org/docs/user-guide/configuring.html#specifying-environments
   */
  env: {
    browser: true,
    node: true
  },
  extends: [
    '@jdi/eslint-config-standard'
  ]
}

# Orbit Controls

Little update to Orbit Controls from three.js to support ES6 modules

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Getting started](#getting-started)
  - [Installing](#installing)
  - [How to use](#how-to-use)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting started
### Installing

```bash
$ yarn add @jdi/OrbitControls [--dev/-D]
```


### How to use
```js
import THREE from 'three'
import createOrbitcontrols from '@jdi/orbit-controls';

const OrbitControls = createOrbitcontrols(THREE)

const controls = new OrbitControls(camera, renderer.domElement);
controls.enabled = true;
controls.maxDistance = 1500;
controls.minDistance = 0;
```
